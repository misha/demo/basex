# How to test your code?

This project is a simple demonstration of how software code can be tested.\
Software tests improve your project stability, maintenability, accessibility and resilience.

XQuery is the programming language used here to highlight software tests core concepts. Tests can however be implemented in every programming language (for example [here](https://gitlab.huma-num.fr/misha/demo/pytest) in Python, or [here](https://gitlab.huma-num.fr/misha/demo/phpunit) in PHP).


## Requirements

This project uses BaseX XQuery Processor.

* [BaseX Main Page](https://docs.basex.org/)
* [Unit Module](https://docs.basex.org/wiki/Unit_Module)

Of course, if you use another XQuery Processor, [alternatives exist](https://stackoverflow.com/questions/513895/unit-testing-xquery).

## Use this project

To run tests, you can use this command:

```
basex -c"<test path='test/BooksTest.xqm'/>"
```

The output will look like this:

```
<testsuites time="PT0.057S">
  <testsuite name="file://BooksTest.xqm" time="PT0.055S" tests="2" failures="0" errors="0" skipped="0">
    <testcase name="get-lower-than-30" time="PT0.029S"/>
    <testcase name="get-lower-than-42" time="PT0.012S"/>
  </testsuite>
</testsuites>

```

## What does it mean?

* The functionnal scope of is sample application is fully tested, with 3 unit tests. This is a guarantee of the code quality: software is tested automatically, with no room for human errors or oversights.\
  Running tests via [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) is a good practice.
* Tests can be thematically grouped in modules/files called tests _suites_.
  This sample project only contains one suite called `books-tests`, defined in the file `BooksTest.xq`.
* When you give each test method a meaningful name, you document the behavior being verified.
  This improves your project accessibility and maintainability.
* When sharing not only your software, but what your software depends on and what its behaviour should be, everyone can more easily install and run your code, and of course validate it.
  This improves your project reproducibility and reusability.

## Repository breakdown

- `README.md` is this file, what you're reading now.  
  It's written in [CommonMark/Markdown](https://commonmark.org/help/).
- `src/Books.xq` is your application code, aka the stuff that does interesting things.  
Here it's a simple XQuery file implementing simple behaviour ; of course, in a real-world scenario, the `src` folder could contain an arbitrarily complex application code.
- `test/BooksTest.xq` is your test code, aka the stuff that validates that your application code behaves as you intend it to.  
  Here we only test different inputs produce different outputs, because it's the most important and I don't want to clutter this sample demo repository.
  We could of course validate our whole application code by adding more tests.
  And in a real-world scenario, the more complex our application code would be, the more tests we would have to implement to make sure our code has no defect.  
  Please note that the test file name is the application file name with "Test" at the end, and that each of the 2 tests methods has a readable name that document a specific behaviour the application must display for the software to be correct.
- `test/books.xml` is just some simple fake/test data that is fed to our application code by our test code.
  This makes our test code independent from our "real" application data.
  This allows us to _not_ having to update test code each time new real-world data appears.

As a side note, we use [git](https://git-scm.com/), and thus each modification of our project creates a new version of it, with a unique identifier.

Here you are : software sharing, testing, validation, documentation, reporting, reusability, reproducibility, tracability, citability, in less than 50 lines of code.

_drops mic_
