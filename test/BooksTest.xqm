module namespace test = 'http://basex.org/modules/books-tests';
import module namespace books = 'http://basex.org/modules/Books' at '../src/Books.xqm';

declare %unit:test function test:get-lower-than-30() {
  let $data := books:get('../test/books.xml', 30)
  let $expected := 
    <results>
      <title lang="en">Learn XPath in 24 Hours</title>
    </results>
    return unit:assert-equals($data, $expected)
};

declare %unit:test function test:get-lower-than-42() {
  let $data := books:get('../test/books.xml', 42)
  let $expected := 
    <results>
      <title lang="en">Learn XPath in 24 Hours</title>
      <title lang="en">Learn Java in 24 Hours</title>
      <title lang="en">Learn Python in 2 Hours</title>
    </results>
    return unit:assert-equals($data, $expected)
};
