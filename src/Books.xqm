module namespace m = 'http://basex.org/modules/Books';

declare function m:get($file, $max) {
  let $books := doc($file)/books/book
  let $results := (
    for $x in $books
      where $x/price < $max
      order by $x/price
      return $x/title
  )
  return <results>{$results}</results>
};
